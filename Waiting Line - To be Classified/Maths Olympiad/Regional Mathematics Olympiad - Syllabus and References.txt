The syllabus for regional mathematics olympiad (RMO) :

The syllabus for regional mathematics olympiad is pre-degree college mathematics.
The areas covered arearithmetic of integers, geometry,quadratic equations and expressions,
trigonometry, co-ordinate geometry, systems of linear equations,permutations and combinations,
factorisation of polynomials,inequalities, elementary combinatorics, probability theory
and number theory,finite series and complex numbers and elementary graph theory.
The syllabus does not include Calculus and Statistics. The major areas fromwhich problems are
given are number theory, geometry,algebra and combinatorics. The syllabus is in a sense spread
over Class IX to Class XII levels, but the problems under each topic are of exceptionally high
level in difficulty and sophistication. The difficulty level increases from RMO to INMO to IMO.

References :

A good idea of what is expected of students in mathematics olympiads
can be had from the following books :

1. Mathematics Olympiad Primer
by V. Krishnamurthy, C.R. Pranesachar,K.N. Ranganathan and B.J. Venkatachala
(Interline Publishing Pvt. Ltd., Bangalore)

2. Challenge and Thrill of Pre-College mathematics 
by V. Krishnamurthy, C.R. Pranesachar, K.N. Ranganathan and B.J. Venkatachala
(New Age International Publishers, New Delhi - 1996).

3. Problem Primer for the Olympiad
by C.R. Pranesachar, B.J. Venkatachala and C.S. Yogananda
(Interline Publishing Pvt. Ltd., Bangalore)

4. An Excursion in Mathematics
by Regional Mathematical Olympiads Committee (Maharashtra and Goa)

